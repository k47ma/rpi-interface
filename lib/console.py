#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re
import pygame
from lib.util import get_font_width, get_font_height


class Console:
    def __init__(self, width, height, font,
                 text_color=(255, 255, 255),
                 background_color=(0, 0, 0), background_alpha=255,
                 line_padding=2, text_margin=3):
        super(Console, self).__init__()

        self.width = width
        self.height = height
        self.font = font
        self.text_color = text_color
        self.background_color = background_color
        self.background_alpha = background_alpha
        self.line_padding = line_padding
        self.text_margin = text_margin

        self._colors = {
            "black": (0, 0, 0),
            "white": (255, 255, 255),
            "gray": (100, 100, 100),
            "lightgray": (75, 75, 75),
            "night": (50, 50, 50),
            "green": (0, 255, 0),
            "darkgreen": (50, 205, 50),
            "red": (255, 0, 0),
            "blue": (30, 144, 255),
            "lightblue": (0, 191, 255),
            "yellow": (255, 255, 0),
            "orange": (255, 165, 0)
        }

        self._font_width = get_font_width(self.font)
        self._font_height = get_font_height(self.font)

        self._background_surface = pygame.Surface((self.width, self.height))
        self._background_surface.fill(self.background_color)
        self._background_surface.set_alpha(self.background_alpha)

        self._surface = pygame.Surface((self.width, self.height), pygame.SRCALPHA, 32)
        self._surface.fill((0, 0, 0, 0))
        self._surface.blit(self._background_surface, (0, 0))

        self._total_rows = ((self.height - self.text_margin) //
                            (self._font_height + self.line_padding))
        self._total_cols = (self.width - self.text_margin) // self._font_width
        self._content = []
        self._modified = False

    def _get_color(self, color, default_color=(255, 255, 255)):
        if color is None:
            return default_color
        color_code = self._colors.get(color)
        if color_code is None:
            return default_color
        return color_code

    def _parse_line(self, line):
        command = {'color': self.text_color,
                   'clear': False,
                   'clearline': False}

        match_result = re.match(r"\{((\w+)(,\w+)*)?\}", line)
        if match_result is None:
            return line, command

        commands = match_result.group(0)
        for word in commands[1:-1].split(','):
            if word == 'clear':
                command['clear'] = True
            elif word == 'clearline':
                command['clearline'] = True
            elif word in self._colors.keys():
                command['color'] = word
        
        return line[len(commands):], command

    def get_surface(self):
        if not self._modified:
            return self._surface

        self._surface.fill((0, 0, 0, 0))
        self._surface.blit(self._background_surface, (0, 0))

        text_x = self.text_margin
        text_y = self.text_margin

        for line_surface in self._content:
            self._surface.blit(line_surface, (text_x, text_y))
            text_y += self.line_padding + self._font_height

        self._modified = False

        return self._surface

    def add_line(self, line):
        self._modified = True

        line, commands = self._parse_line(line)

        if commands['clear']:
            self.clear()
            return
        elif commands['clearline']:
            if self._content:
                del self._content[-1]

        nrows = len(line) // self._total_cols
        if len(line) % self._total_cols > 0:
            nrows += 1

        for row_ind in range(nrows):
            subline = line[row_ind * self._total_cols:
                           (row_ind + 1) * self._total_cols]
            rendered_text = self.font.render(subline, True, self._get_color(commands['color']))
            self._content.append(rendered_text)

        while len(self._content) > self._total_rows:
            del self._content[0]

    def clear(self):
        self._content = []
        self._modified = True
