#!/usr/bin/python3

import sys
import time

commands = ["", "green", "blue", "orange", "yellow"]
for i in range(5):
    sys.stdout.write("{{{0}}}iteration {1}\n".format(commands[i], i + 1))
    sys.stdout.flush()
    time.sleep(1)

sys.stdout.write("{clear}\n")
sys.stdout.flush()

for i in range(5):
    sys.stdout.write("{clearline}\n")
    sys.stdout.flush()
    sys.stdout.write("{{{0}}}iteration {1}\n".format(commands[i], i + 1))
    sys.stdout.flush()
    time.sleep(1)
