#!/usr/bin/python3

import sys
import time

for i in range(5):
    sys.stdout.write("iteration {}\n".format(i + 1))
    sys.stdout.flush()
    time.sleep(1)

sys.stderr.write("some error message!")
sys.stderr.flush()

sys.exit(-1)
